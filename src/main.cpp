#include <Arduino.h>
#include <Esp.h>

#include <Ticker.h>
#include <WiFiManager.h>

#include "MQTTClientHandler.h"
#include <ArduinoJson.h>
#include <NtpClientLib.h>
#include <TimeLib.h>

#include <MadgwickAHRS.h>
#include <SparkFunLSM9DS1.h>
#include <Wire.h>

#include <Microphone.h>
#include <PulseSensor.h>

#include <ESPPersistentStorage.h>
#include <ESPTimeseriesDB.h>

#define IMU_SAMPLEFREQ 500

#define PM_SAMPLEFREQ 500

#define PULSE_ANALOG_IN 33
#define PULSE_ENABLE 32
#define PULSE_LED 0

#define MIC_ANALOG_IN 35

#define CONNECTED_LED 2

#define DATARATE_INTERVAL 15
#define DATASYNC_INTERVAL 120

String NAME = "[SleepTracker] ";

#define PRINT(...)            \
  Serial.print(NAME);         \
  Serial.printf(__VA_ARGS__); \
  Serial.println();           \
  Serial.flush();

WiFiManager wifiManager;
Ticker ticker;

ESPTimeseriesDB database;
ESPPersistentStorage persistent;

AsyncMqttClient mqttClient;
time_t lastDataSync;

static LSM9DS1 imu;
static MadgwickAHRS AHRS(0.2f);
Quaternion diffSum(0, 0, 0, 0);

Quaternion lastQ;
VectorStruct absAccel;

static PulseSensor pulseSensor(PULSE_ANALOG_IN, PULSE_LED);
static Microphone microphone(MIC_ANALOG_IN);

hw_timer_t *imuTimer = NULL;
hw_timer_t *pmTimer  = NULL;

bool imu_ready          = false;
bool wifiFirstConnected = false;

static uint32_t cp0_regs[18];

void IRAM_ATTR updateIMU(void) {
  uint32_t cp_state = xthal_get_cpenable();   // get FPU state
  if (cp_state) {
    xthal_save_cp0(cp0_regs);   // Save FPU registers
  } else {
    xthal_set_cpenable(1);   // enable FPU
  }

  if (imu.gyroAvailable()) {
    imu.readGyro();
    if (imu.accelAvailable()) {
      imu.readAccel();
    }
    if (imu.magAvailable()) {
      imu.readMag();
    }
    AHRS.update((M_PI / 180) * imu.calcGyro(imu.gx),   //
                (M_PI / 180) * imu.calcGyro(imu.gy),   //
                (M_PI / 180) * imu.calcGyro(imu.gz),   //
                imu.calcAccel(imu.ax),                 //
                imu.calcAccel(imu.ay),                 //
                imu.calcAccel(imu.az),                 //
                imu.calcMag(imu.mx),                   //
                imu.calcMag(imu.my),                   //
                imu.calcMag(imu.mz));
    auto linAcc = AHRS.getRotation().conj().rotateVector(
        imu.calcAccel(imu.ax), imu.calcAccel(imu.ay), imu.calcAccel(imu.az));
    linAcc.z -= 1;
    absAccel.x += abs(linAcc.x);
    absAccel.y += abs(linAcc.y);
    absAccel.z += abs(linAcc.z);

    Quaternion q = AHRS.getRotation();
    diffSum += q * lastQ.conj();
    lastQ = q;

    imu.gx = imu.gy = imu.gz = 0.0f;
    imu.ax = imu.ay = imu.az = 0.0f;
    imu.mx = imu.my = imu.mz = 0.0f;
  }

  if (cp_state) {
    xthal_restore_cp0(cp0_regs);   // Restore FPU registers
  } else {
    xthal_set_cpenable(0);   // turn it back off
  }
}

void startIMU() {
  imu.settings.device.commInterface = IMU_MODE_I2C;
  imu.settings.device.mAddress      = 0x1E;
  imu.settings.device.agAddress     = 0x6B;

  imu.settings.gyro.scale          = 245;     // Set scale to +/-245dps
  imu.settings.gyro.sampleRate     = 4;       // 119 Hz
  imu.settings.gyro.lowPowerEnable = false;   // LP mode off
  imu.settings.gyro.HPFEnable      = true;    // HPF disabled
  imu.settings.gyro.HPFCutoff      = 1;       // HPF cutoff = 4Hz
                                              //
  imu.settings.accel.scale         = 2;       // Set accel scale to +/-8g.
  imu.settings.accel.sampleRate    = 4;       // Set accel to 119 Hz.
  imu.settings.accel.highResEnable = false;   // Disable HR
                                              //
  imu.settings.mag.scale          = 4;        // Set mag scale to +/-4 Gs
  imu.settings.mag.sampleRate     = 7;        // Set OD rate to 80Hz
  imu.settings.mag.XYPerformance  = 0;        // Low power mode
  imu.settings.mag.ZPerformance   = 0;        // Ultra-high perform.
  imu.settings.mag.lowPowerEnable = true;     //
  imu.settings.mag.operatingMode  = 0;        // Continuous mode

  Wire.setTimeout(2000);
  if (!imu.begin()) {
    PRINT("Unable to initialize the LSM9DS1. Check your wiring!");
    ESP.restart();
    imu_ready = false;
    return;
  }

  imu.calibrate();
  imu.readAccel();
  imu.readMag();
  for (uint8_t i = 0; i < 100; i++) {
    AHRS.update(0, 0, 0,   //
                imu.calcAccel(imu.ax), imu.calcAccel(imu.ay),
                imu.calcAccel(imu.az),   //
                imu.calcMag(imu.mx), imu.calcMag(imu.my), imu.calcMag(imu.mz));
  }

  // -> use a scaling of 80 results in 1 000 000 times per second -> 1us
  imuTimer = timerBegin(0, 80, true);
  timerAttachInterrupt(imuTimer, &updateIMU, true);
  timerAlarmWrite(imuTimer, 1000000ul / IMU_SAMPLEFREQ, true);

  imu_ready = true;

  PRINT("LSM9DS1 9DOF initialized!");
}

#define NUM_VALS 5
int counter = 0;
int pulse   = 0;
int mic     = 0;

void printRawData(void) {
  counter++;
  counter = counter % NUM_VALS;
  pulse += pulseSensor.getLatestSample();
  mic += microphone.getLastSample();
  if (counter == 0) {
    Serial.print(pulse / NUM_VALS);
    Serial.print(",");
    Serial.print(mic / NUM_VALS);
    Serial.println();
    pulse = 0;
    mic   = 0;
  }
}

void IRAM_ATTR updatePulseAndMic(void) {
  pulseSensor.update();
  microphone.update();

  // printRawData();
}

void startPulseAndMic() {
  pinMode(MIC_ANALOG_IN, INPUT);

  pulseSensor.begin();
  microphone.begin();

  // -> use a scaling of 80 results in 1 000 000 times per second -> 1us
  pmTimer = timerBegin(1, 80, true);
  timerAttachInterrupt(pmTimer, &updatePulseAndMic, true);
  timerAlarmWrite(pmTimer, 1000000ul / PM_SAMPLEFREQ, true);
  PRINT("PulseSensor initialized!");
}

void syncTime() {
  NTP.onNTPSyncEvent([](NTPSyncEvent_t ntpEvent) {
    if (ntpEvent) {
      PRINT("Time Sync error: ");
      if (ntpEvent == noResponse) {
        PRINT("NTP server not reachable");
      } else if (ntpEvent == invalidAddress) {
        PRINT("Invalid NTP server address");
      }
    } else {
      digitalWrite(CONNECTED_LED, HIGH);
      PRINT("Got NTP time: %s(%lu)",
            NTP.getTimeDateString(NTP.getLastNTPSync()).c_str(),
            NTP.getLastNTPSync());
    }
  });

  NTP.begin("pool.ntp.org", 1, true, 0);
  NTP.setInterval(600);
}

void blinkLED() {
  // toggle state
  int state = digitalRead(CONNECTED_LED);   // get the current state of GPIO1 pin
  digitalWrite(CONNECTED_LED, !state);      // set pin to the opposite state
}

bool connected(void) { return WiFi.isConnected(); }

void connect(void) {
  if (connected())
    return;
  PRINT("Connecting to WiFi network..");
  ticker.attach(1, blinkLED);
  wifiManager.autoConnect("SleepTracker", "mySleepTracker");
  ticker.detach();
  digitalWrite(CONNECTED_LED, HIGH);
  PRINT("Connected to WiFi network!");
}

void disconnect(void) {
  wifiManager.disconnect();
  digitalWrite(CONNECTED_LED, LOW);
  PRINT("Disconnected from WiFi network!");
}

void stopInterrupts(void) {
  timerAlarmDisable(imuTimer);
  timerAlarmDisable(pmTimer);
}
void startInterrupts(void) {
  timerAlarmEnable(imuTimer);
  timerAlarmEnable(pmTimer);
}

void onMQTTConnected(bool sessionPresent) {
  PRINT("Connected to MQTT, sending data...");
  stopInterrupts();
  auto stream = database.getStream(lastDataSync);
  uint16_t len = stream.available();
  char *buffer = new char[len];
  stream.readBytes(buffer, len);
  ticker.attach(0.25, blinkLED);
  mqttClient.publish("datatest", 0, false, buffer, len);
}

void onMQTTPublished(uint8_t packetId) {
  PRINT("Data sent! disconnecting...");
  ticker.detach();
  digitalWrite(CONNECTED_LED, HIGH);
  lastDataSync = now();
  persistent.set("ts", (uint8_t *)&lastDataSync, sizeof(lastDataSync));
  startInterrupts();
  mqttClient.disconnect();
}

void onMQTTDisconnected(AsyncMqttClientDisconnectReason reason) {
  disconnect();
}

void setup() {
  Serial.begin(115200);
  PRINT("System started");

  SPIFFS.begin(true);
  PRINT("Filesystem initialized");
  database.begin();
  PRINT("Database initialized");

  if (!persistent.get("ts", (uint8_t *)&lastDataSync, sizeof(lastDataSync))) {
    lastDataSync = 0;
  }
  lastDataSync = 0;
  PRINT("Next data sync in: %lu", (lastDataSync + DATASYNC_INTERVAL));

  startIMU();
  startPulseAndMic();

  pinMode(CONNECTED_LED, OUTPUT);
  pinMode(PULSE_ENABLE, OUTPUT);
  digitalWrite(CONNECTED_LED, LOW);
  digitalWrite(PULSE_ENABLE, LOW);

  mqttClient.setServer("sleep.heim.xyz", 1883);
  mqttClient.setCredentials("sensor01", "heartratesensor");
  mqttClient.onConnect(onMQTTConnected);
  mqttClient.onDisconnect(onMQTTDisconnected);
  mqttClient.onPublish(onMQTTPublished);

  connect();
  syncTime();
  delay(0);
  startInterrupts();
  PRINT("Going into runtime!");
}

void loop() {
  PRINT("Storing data...");
  digitalWrite(CONNECTED_LED, HIGH);
  // StaticJsonBuffer<512> jsonBuffer;
  // JsonArray &jarray = jsonBuffer.createArray();
  // JsonObject &jroot = jarray.createNestedObject();
  auto currentTime = now();
  // jroot["time"] = currentTime;

  stopInterrupts();
  if (pulseSensor.getBeatsPerMinute() != 0) {
    // JsonObject &jobj = jroot.createNestedObject("Pulse");
    // jobj["BPM"] = pulseSensor.getBeatsPerMinute();
    database.push(currentTime, "bmp", pulseSensor.getBeatsPerMinute());
  }

  uint16_t max = microphone.getMax();
  if (max != 0) {
    // JsonObject &jobj = jroot.createNestedObject("Microphone");
    // jobj["MAX"] = max;
    // jobj["RMS"] = microphone.getRMS();
    database.push(currentTime, "micro_vol_max", max);
    database.push(currentTime, "micro_vol", microphone.getRMS());
  }

  if (imu_ready) {
    float linAcc     = sqrt(absAccel.x * absAccel.x + absAccel.y * absAccel.y +
                        absAccel.z * absAccel.z);
    float angularVel = sqrt(diffSum[0] * diffSum[0] + diffSum[1] * diffSum[1] +
                            diffSum[2] * diffSum[2] + diffSum[3] * diffSum[3]);
    // JsonObject &jobj = jroot.createNestedObject("Acc");
    // jobj["LinAcc"] = linAcc;
    // jobj["angularVel"] = angularVel;
    absAccel.x = 0;
    absAccel.y = 0;
    absAccel.z = 0;
    diffSum    = Quaternion(0, 0, 0, 0);
    database.push(currentTime, "lin_acc", linAcc);
    database.push(currentTime, "angular_change", angularVel);
  }
  startInterrupts();
  digitalWrite(CONNECTED_LED, LOW);

  if (lastDataSync + DATASYNC_INTERVAL <= now()) {
    PRINT("Syncing data...");
    connect();
    PRINT("Connecting to MQTT...");
    if (!mqttClient.connected()) {
      mqttClient.connect();
    }
  }
  delay(DATARATE_INTERVAL * 1000);
}
