#include "MQTTClientHandler.h"

using namespace std::placeholders;

MQTTClientHandler::MQTTClientHandler() : _client(new AsyncMqttClient()) {
  _registerHandles();
}

MQTTClientHandler::~MQTTClientHandler() { delete _client; }

void MQTTClientHandler::_registerHandles() {
  _client->onConnect(std::bind(&MQTTClientHandler::_onConnected, this, _1));
  _client->onDisconnect(
      std::bind(&MQTTClientHandler::_onDisconnected, this, _1));
  _client->onSubscribe(
      std::bind(&MQTTClientHandler::_onSubscribe, this, _1, _2));
  _client->onUnsubscribe(
      std::bind(&MQTTClientHandler::_onUnsubscribe, this, _1));
  _client->onMessage(
      std::bind(&MQTTClientHandler::_onMessage, this, _1, _2, _3, _4, _5, _6));
  _client->onPublish(std::bind(&MQTTClientHandler::_onPublish, this, _1));
}

void MQTTClientHandler::_onConnected(bool sessionPresent) {
  Serial.println("Connected to MQTTClientHandler.");
  Serial.print("Session present: ");
  Serial.println(sessionPresent);
  // uint16_t packetIdSub = _client->subscribe("test/lol", 2);
  // Serial.print("Subscribing at QoS 2, packetId: ");
  // Serial.println(packetIdSub);
  // _client->publish("test/lol", 0, true, "test 1");
  // Serial.println("Publishing at QoS 0");
  // uint16_t packetIdPub1 = _client->publish("test/lol", 1, true, "test 2");
  // Serial.print("Publishing at QoS 1, packetId: ");
  // Serial.println(packetIdPub1);
  // uint16_t packetIdPub2 = _client->publish("test/lol", 2, true, "test 3");
  // Serial.print("Publishing at QoS 2, packetId: ");
  // Serial.println(packetIdPub2);
}

void MQTTClientHandler::_onDisconnected(
    AsyncMqttClientDisconnectReason reason) {
  Serial.println("Disconnected from MQTTClientHandler.");
}

void MQTTClientHandler::_onSubscribe(uint16_t packetId, uint8_t qos) {
  Serial.println("Subscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
  Serial.print("  qos: ");
  Serial.println(qos);
}

void MQTTClientHandler::_onUnsubscribe(uint16_t packetId) {
  Serial.println("Unsubscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}

void MQTTClientHandler::_onMessage(char *topic, char *payload,
                                   AsyncMqttClientMessageProperties properties,
                                   size_t len, size_t index, size_t total) {
  Serial.println("Publish received.");
  Serial.print("  topic: ");
  Serial.println(topic);
  Serial.print("  qos: ");
  Serial.println(properties.qos);
  Serial.print("  dup: ");
  Serial.println(properties.dup);
  Serial.print("  retain: ");
  Serial.println(properties.retain);
  Serial.print("  len: ");
  Serial.println(len);
  Serial.print("  index: ");
  Serial.println(index);
  Serial.print("  total: ");
  Serial.println(total);
}

void MQTTClientHandler::_onPublish(uint16_t packetId) {
  Serial.println("Publish acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}
