/*
   PulseSensor measurement manager.
   See https://www.pulsesensor.com to get started.

   Copyright World Famous Electronics LLC - see LICENSE
   Contributors:
     Joel Murphy, https://pulsesensor.com
     Yury Gitman, https://pulsesensor.com
     Bradford Needham, @bneedhamia, https://bluepapertech.com

   Licensed under the MIT License, a copy of which
   should have been included with this software.

   This software is not intended for medical use.
*/
#include "PulseSensor.h"

/*
   Internal constants controlling the rate of fading for the _ledPin.

   FADE_SCALE = _fadeLevel / FADE_SCALE is the corresponding PWM value.
   FADE_LEVEL_PER_SAMPLE = amount to decrease _fadeLevel per sample.
   MAX_FADE_LEVEL = maximum _fadeLevel value.

   The time (milliseconds) to fade to black =
     (MAX_FADE_LEVEL / FADE_LEVEL_PER_SAMPLE) * sample time (2ms)
*/
#define FADE_SCALE 10
#define FADE_LEVEL_PER_SAMPLE 12
#define MAX_FADE_LEVEL (255 * FADE_SCALE)

static const uint16_t FIRCoeffs[12] = {172,  321,  579,  927,  1360, 1858,
                                       2390, 2916, 3391, 3768, 4012, 4096};

/*
   Constructs a Pulse detector that will process PulseSensor voltages
   that the caller reads from the PulseSensor.
*/
PulseSensor::PulseSensor(int8_t inputPin, int8_t ledPin, bool fadeLED)
    : _inputPin(inputPin), _ledPin(ledPin), _fadeLED(fadeLED) {}

bool PulseSensor::begin(void) {

  for (uint8_t i = 0; i < 10; i++) {
    _IBIs[i] = 0;
  }
  if (_ledPin >= 0) {
    if (_fadeLED) {
      pinMode(_ledPin, OUTPUT);
      analogWrite(_ledPin, 0); // turn off the LED.
    } else {
      pinMode(_ledPin, OUTPUT);
      digitalWrite(_ledPin, LOW);
    }
  }
  if (_inputPin >= 0) {
    pinMode(_inputPin, INPUT);
    return true;
  }
  return false;
}

void PulseSensor::update(void) {
  if (_inputPin <= 0) {
    return;
  }

  unsigned long sampleTime = millis();
  uint32_t currentIBI = sampleTime - _lastBeatTime;

  _updateLEDs();

  _prevSignal = _currentSignal;
  _currentSignal = analogRead(_inputPin);
  _currentSignal -= _estimateAverage(_currentSignal, &_avgSignalEstimator);
  _currentSignal = _lowPassFilter(_currentSignal);

  if ((_prevSignal < 0) && (_currentSignal >= 0)) {

    if (_maxPeak > _max / 2)
      _max = _maxPeak; // Adjust our AC max and min
    else
      _max = (int16_t)(_max * 3 / 4);
    _positiveEdge = true;
    _maxPeak = 0;
  }

  //  Detect negative zero crossing (falling edge)
  if ((_prevSignal > 0) && (_currentSignal <= 0)) {

    if (_minPeak < _min / 4)
      _min = _minPeak;
    else
      _min = (int16_t)(_min * 3 / 4);

    _positiveEdge = false;
    _minPeak = 0;

    if (_maxPeak > (_max * 3 / 4) && _max > _min / -2) {
      // Heart beat!!!
      if (_lastBeatTime > 0) {
        _IBIs[_IBIsIterator++] = _lastBeatTime - _lastPeakTime;
        _IBIsIterator %= 10;
      }
      _lastBeatTime = _lastPeakTime;

      int32_t IBIAvg = 0;
      uint8_t samples = 0;
      for (uint8_t i = 0; i < 10; i++) {
        if (_IBIs[i] != 0) {
          IBIAvg += _IBIs[i];
          samples++;
        }
      }
      IBIAvg /= samples;
      if (samples > 3) {
        _currentBPM = 60000 / IBIAvg;
        _fadeLevel = MAX_FADE_LEVEL;
      }
    }
  }

  //  Find Maximum value in positive cycle
  if (_positiveEdge) {
    if (_currentSignal > _maxPeak) {
      _maxPeak = _currentSignal;
      _lastPeakTime = sampleTime;
      if (_maxPeak > _max)
        _max = _maxPeak;
    }
  }
  //  Find Minimum value in negative cycle
  if (!_positiveEdge) {
    if (_currentSignal < _prevSignal) {
      _minPeak = _currentSignal;
      if (_minPeak < _min)
        _min = _minPeak;
    }
  }

  if (_IBIsIterator != 0 && sampleTime - _lastBeatTime > 2000) {
    // We did not see a pulse for a long time. reset.
    _currentBPM = 0;
    _lastBeatTime = 0;
  }
}

int16_t PulseSensor::getLatestSample() { return _currentSignal; }

uint8_t PulseSensor::getBeatsPerMinute() { return _currentBPM; }

void PulseSensor::_updateLEDs() {
  if (_ledPin >= 0) {
    if (_fadeLevel > 0) {
      _fadeLevel = _fadeLevel - FADE_LEVEL_PER_SAMPLE;
      _fadeLevel = constrain(_fadeLevel, 0, MAX_FADE_LEVEL);
    }
    if (_fadeLED) {
      analogWrite(_ledPin, _fadeLevel / FADE_SCALE);
    } else {
      digitalWrite(_ledPin, _fadeLevel > 0);
    }
  }
}

//  Average DC Estimator
int16_t PulseSensor::_estimateAverage(uint16_t x, int64_t *estimator) {
  *estimator += ((((uint32_t)x << 15) - *estimator) >> 10);
  return (*estimator >> 15);
}

//  Low Pass FIR Filter
int16_t PulseSensor::_lowPassFilter(int16_t dataIn) {
  _dataBuffer[_dataBufferIterator] = dataIn;

  int32_t z = FIRCoeffs[11] * (_dataBuffer[(_dataBufferIterator - 11) & 0x1F]);

  for (uint8_t i = 0; i < 11; i++) {
    z += FIRCoeffs[i] * (_dataBuffer[(_dataBufferIterator - i) & 0x1F] +
                         _dataBuffer[(_dataBufferIterator - 22 + i) & 0x1F]);
  }

  _dataBufferIterator++;
  _dataBufferIterator %= 32; // Wrap condition

  return (z >> 15);
}
