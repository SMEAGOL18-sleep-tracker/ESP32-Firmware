
#include "Arduino.h"
#undef min
#undef max

#include "DBStream.h"
#include "ESPTimeseriesDB.h"

#define BUFFER_LEN 32
#define SECS_YR_2000 (946684800UL)   // the time at the start of y2k

DBStream::DBStream(ESPTimeseriesDB *db, std::vector<File> files, const uint32_t startOfFirstFile, std::vector<bool> filterIDs, DBStreamEncoding encoding)
    : _db(db),
      _filterIDs(filterIDs),
      _files(files),
      _startOfFirstFile(startOfFirstFile),
      _encoding(encoding),
      _filesIndex(0),
      _buffer(new uint8_t[BUFFER_LEN]) {
  _timeout = 0;
  reset();
};

DBStream::~DBStream() {
  delete[] _buffer;
  for (auto &file : _files) {
    file.close();
  }
}

size_t DBStream::write(uint8_t data) {
  return 0;
}

int DBStream::available() {
  return _totalLen;
}

int DBStream::read() {
  auto ch = peek();
  if (ch != -1) {
    _bufferPos++;
    _totalLen--;
  }
  return ch;
}

int DBStream::peek() {
  while (_bufferPos >= _bufferLen) {
    if (!_updateBuffer()) {
      return -1;
    }
  }
  return _buffer[_bufferPos];
}

void DBStream::flush() {
}

bool DBStream::_updateBuffer(void) {
  _bufferLen = 0;
  File *file = _getCurrentFile();
  if (_encoding == DBStreamEncoding::Json && _filesIndex == 0 && _filePos == 0) {
    _buffer[_bufferLen++] = '{';
    if (_fileSize == 0) {
      _buffer[_bufferLen++] = '}';
      _filePos += 1;
      _bufferPos = 0;
      return true;
    }
  }
  if (file == nullptr) {
    return false;
  }
  if (_filePos < _fileSize) {
    if ((uint8_t)file->peek() == TS_TIME_ID) {
      // check if the timestamps contains values (given the filters)
      while (!_fieldContainsValues(file)) {
        do {   // search for the next timestamp
          file->seek(5, SeekMode::SeekCur);
          if (file->position() >= file->size()) {
            // we reached the end of the file, start with a fresh one
            return true;
          }
        } while ((uint8_t)file->peek() != TS_TIME_ID);
      }
      uint32_t ts = 0;
      //check if the timestamp is valid.. at least from year 2000
      file->seek(1, SeekMode::SeekCur);
      file->read((uint8_t *)&ts, 4);
      while (ts < SECS_YR_2000) {
        do {   // search for the next timestamp
          file->seek(5, SeekMode::SeekCur);
          if (file->position() >= file->size()) {
            // we reached the end of the file, start with a fresh one
            return true;
          }
        } while ((uint8_t)file->peek() != TS_TIME_ID);
        file->seek(1, SeekMode::SeekCur);
        file->read((uint8_t *)&ts, 4);
      }
      if (_encoding == DBStreamEncoding::Json) {
        const String timestamp = String(ts);
        // check if we need to add a comma. we print it first, this way
        // we don't need to print it for the first value
        if (_filesIndex != 0 || _filePos != 0) {
          _buffer[_bufferLen++] = '}';
          _buffer[_bufferLen++] = ',';
        }
        _buffer[_bufferLen++] = '\"';
        strncpy((char *)&_buffer[_bufferLen], timestamp.c_str(), timestamp.length());
        _bufferLen += timestamp.length();
        _buffer[_bufferLen++] = '\"';
        _buffer[_bufferLen++] = ':';
        _buffer[_bufferLen++] = '{';
      } else if (_encoding == DBStreamEncoding::Raw) {
        file->seek(file->position() - 5);
        _bufferLen = file->read((uint8_t *)_buffer, 5);
      }
    } else {
      uint8_t keyID = (uint8_t)file->peek();
      // check if the value is filtered
      if (keyID == TS_INVALID_KEY_ID || keyID >= TS_END_ID ||   //
          (_filterIDs.size() != 0 && (keyID >= _filterIDs.size() || _filterIDs[keyID] == false))) {
        file->seek(5, SeekMode::SeekCur);
        _filePos += 5;
        return true;
      }
      if (_encoding == DBStreamEncoding::Json) {
        // check if we need to add a comma. we print it first, this way
        // we don't need to print it for the first value after the timestamp
        if (_buffer[_bufferPos - 1] != '{') {
          _buffer[_bufferLen++] = ',';
        }
        String key       = _db->_getKey(keyID);
        uint16_t key_len = key.length() > TS_MAX_KEY_LENGTH ? TS_MAX_KEY_LENGTH : key.length();

        file->seek(1, SeekMode::SeekCur);
        float val = 0;
        file->read((uint8_t *)&val, 4);
        const String value = String(val);
        uint16_t value_len = value.length();
        value_len          = constrain(value_len, 0, BUFFER_LEN - _bufferLen - 1 - key_len - 3 - value_len);

        _buffer[_bufferLen++] = '\"';
        strncpy((char *)&_buffer[_bufferLen], key.c_str(), key_len);
        _bufferLen += key_len;
        _buffer[_bufferLen++] = '\"';
        _buffer[_bufferLen++] = ':';
        strncpy((char *)&_buffer[_bufferLen], value.c_str(), value_len);
        _bufferLen += value_len;
      } else if (_encoding == DBStreamEncoding::Raw) {
        _bufferLen = file->read((uint8_t *)_buffer, 5);
      }
    }
    _filePos += 5;
  }
  if (_filePos == _fileSize) {
    if (_encoding == DBStreamEncoding::Json && _filesIndex == _files.size() - 1) {
      _buffer[_bufferLen++] = '}';
      _buffer[_bufferLen++] = '}';
    }
    _filePos += 1;
  }
  _buffer[_bufferLen] = '\0';
  _bufferPos          = 0;
  return true;
}

File *DBStream::_getCurrentFile(void) {
  File *file = nullptr;
  if (_filesIndex >= _files.size()) {
    return file;
  }
  if (_filesIndex < _files.size()) {
    file = (File *)&_files[_filesIndex];
    if (_fileSize == 0) {   // only happens for the first file.
      file->seek(_startOfFirstFile);
      _fileSize = file->size() - file->position();
    }
  }
  if (file == nullptr || _filePos > _fileSize || (file->position() % 5 == 0 && file->peek() == TS_END_ID)) {
    _filesIndex++;
    if (_filesIndex == _files.size()) {
      _filesIndex = _files.size();
      _fileSize   = 0;
      return nullptr;
    } else {
      file = (File *)&_files[_filesIndex];
      file->seek(0);
      if (file) {
        _fileSize = file->size();
      }
      _filePos = 0;
    }
  }
  return file;
}

bool DBStream::_fieldContainsValues(File *file) {
  uint32_t prevPos = file->position();
  while (file->position() < file->size()) {
    file->seek(5, SeekCur);
    uint8_t id = file->peek();
    if (id >= TS_TIME_ID) {
      break;
    } else if (_filterIDs.size() == 0 || _filterIDs[id] == true) {
      // we found a value!
      file->seek(prevPos);
      return true;
    }
  }
  file->seek(prevPos);
  return false;
}

void DBStream::reset() {
  _totalLen   = 0;
  _filePos    = 0;
  _fileSize   = 0;
  _filesIndex = 0;
  // calculate the total length of the stream
  while (_updateBuffer()) {
    if (_bufferLen > 0)
      _totalLen += _bufferLen;
    _bufferPos = _bufferLen;
  }
  _filePos    = 0;
  _fileSize   = 0;
  _filesIndex = 0;
}
