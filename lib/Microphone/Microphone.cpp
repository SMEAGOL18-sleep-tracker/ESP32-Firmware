#include "Microphone.h"

Microphone::Microphone(int8_t analogIn) : _analogIn(analogIn) {}

bool Microphone::begin(void) {
  if (_analogIn <= 0) {
    return false;
  }
  pinMode(_analogIn, INPUT);
  return true;
}

void Microphone::update(void) {
  if (_analogIn <= 0) {
    return;
  }
  int32_t sample = analogRead(_analogIn);
  _lastSample = (uint16_t)abs(sample - MIC_MEAN);
  if (_lastSample > _max) {
    _max = _lastSample;
  }
  _RMSSum += (_lastSample * _lastSample);
  _numSamples++;
}

uint16_t Microphone::getLastSample() { return _lastSample; };

uint16_t Microphone::getMax() {
  uint16_t max = _max;
  if (_autoResetMax) {
    resetMax();
  }
  return max;
};

void Microphone::resetMax(void) { _max = 0; }

float Microphone::getRMS() {
  float RMS = _numSamples == 0 ? 0 : sqrt((float)_RMSSum / _numSamples);
  if (_autoResetRMS) {
    resetRMS();
  }
  return RMS;
};

void Microphone::resetRMS(void) {
  _numSamples = 0;
  _RMSSum = 0;
}
