/*
    MadgwickAHRS.cpp
    Author: Benedikt Eliasson

    Arduino C++ class-style implementation of Madgwick's IMU and AHRS
   algorithms. See:
   http://www.x-io.co.uk/node/8#open_source_ahrs_and_imu_algorithms
*/

//-----------------------------------------------------------------------------
// Header files

#include "MadgwickAHRS.h"
#include <math.h>

//-----------------------------------------------------------------------------
// Constructor declaration
MadgwickAHRS::MadgwickAHRS(float beta, float interval)
    : beta(beta), sampleInterval(interval){};

//-----------------------------------------------------------------------------
// AHRS algorithm update

void MadgwickAHRS::update(float gx, float gy, float gz, float ax, float ay,
                          float az, float mx, float my, float mz) {
  Quaternion s, qDot, a, m, h, b;
  float q0q1, q0q2, q0q3, q1q1, q1q2, q1q3, q2q2, q2q3, q3q3;

  // Use IMU algorithm if magnetometer measurement invalid (avoids NaN in
  // magnetometer normalisation)
  if ((mx == 0.0f) && (my == 0.0f) && (mz == 0.0f)) {
    MadgwickAHRS::updateIMU(gx, gy, gz, ax, ay, az);
    return;
  }

  // Rate of change of quaternion from gyroscope
  qDot = 0.5 * (q * Quaternion(0, gx, gy, gz));

  // Compute feedback only if accelerometer measurement valid (avoids NaN in
  // accelerometer normalisation)
  if (!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f))) {

    // Normalise accelerometer measurement
    a = Quaternion(0, ax, ay, az).normalise();

    // Normalise magnetometer measurement
    m = Quaternion(0, mx, my, mz).normalise();

    // Auxiliary variables to avoid repeated arithmetic
    q0q1 = q[0] * q[1];
    q0q2 = q[0] * q[2];
    q0q3 = q[0] * q[3];
    q1q1 = q[1] * q[1];
    q1q2 = q[1] * q[2];
    q1q3 = q[1] * q[3];
    q2q2 = q[2] * q[2];
    q2q3 = q[2] * q[3];
    q3q3 = q[3] * q[3];

    // Reference direction of Earth's magnetic field
    h = q * m * q.conj();
    b = Quaternion(0, sqrt(h[1] * h[1] + h[2] * h[2]), 0, h[3]);

    // Gradient decent algorithm corrective step
    s[0] = -2.0f * q[2] * (2.0f * q1q3 - 2.0f * q0q2 - a[1]) +
           2.0f * q[1] * (2.0f * q0q1 + 2.0f * q2q3 - a[2]) -
           b[3] * q[2] *
               (b[1] * (0.5f - q2q2 - q3q3) + b[3] * (q1q3 - q0q2) - m[1]) +
           (-b[1] * q[3] + b[3] * q[1]) *
               (b[1] * (q1q2 - q0q3) + b[3] * (q0q1 + q2q3) - m[2]) +
           b[1] * q[2] *
               (b[1] * (q0q2 + q1q3) + b[3] * (0.5f - q1q1 - q2q2) - m[3]);
    s[1] = 2.0f * q[3] * (2.0f * q1q3 - 2.0f * q0q2 - a[1]) +
           2.0f * q[0] * (2.0f * q0q1 + 2.0f * q2q3 - a[2]) -
           4.0f * q[1] * (1 - 2.0f * q1q1 - 2.0f * q2q2 - a[3]) +
           b[3] * q[3] *
               (b[1] * (0.5f - q2q2 - q3q3) + b[3] * (q1q3 - q0q2) - m[1]) +
           (b[1] * q[2] + b[3] * q[0]) *
               (b[1] * (q1q2 - q0q3) + b[3] * (q0q1 + q2q3) - m[2]) +
           (b[1] * q[3] - 2.0f * b[3] * q[1]) *
               (b[1] * (q0q2 + q1q3) + b[3] * (0.5f - q1q1 - q2q2) - m[3]);
    s[2] = -2.0f * q[0] * (2.0f * q1q3 - 2.0f * q0q2 - a[1]) +
           2.0f * q[3] * (2.0f * q0q1 + 2.0f * q2q3 - a[2]) -
           4.0f * q[2] * (1 - 2.0f * q1q1 - 2.0f * q2q2 - a[3]) +
           (-2.0f * b[1] * q[2] - b[3] * q[0]) *
               (b[1] * (0.5f - q2q2 - q3q3) + b[3] * (q1q3 - q0q2) - m[1]) +
           (b[1] * q[1] + b[3] * q[3]) *
               (b[1] * (q1q2 - q0q3) + b[3] * (q0q1 + q2q3) - m[2]) +
           (b[1] * q[0] - 2.0f * b[3] * q[2]) *
               (b[1] * (q0q2 + q1q3) + b[3] * (0.5f - q1q1 - q2q2) - m[3]);
    s[3] = 2.0f * q[1] * (2.0f * q1q3 - 2.0f * q0q2 - a[1]) +
           2.0f * q[2] * (2.0f * q0q1 + 2.0f * q2q3 - a[2]) +
           (-2.0f * b[1] * q[3] + b[3] * q[1]) *
               (b[1] * (0.5f - q2q2 - q3q3) + b[3] * (q1q3 - q0q2) - m[1]) +
           (-b[1] * q[0] + b[3] * q[2]) *
               (b[1] * (q1q2 - q0q3) + b[3] * (q0q1 + q2q3) - m[2]) +
           b[1] * q[1] *
               (b[1] * (q0q2 + q1q3) + b[3] * (0.5f - q1q1 - q2q2) - m[3]);

    // Apply feedback step
    qDot -= s.normalise() * beta;
  }

  // calculate the time passed
  float dt = sampleInterval;
  if (dt == 0.0f) {
    unsigned long currentTime = micros();
    dt = (currentTime - lastSampleTime) / 1000000.0f;
    lastSampleTime = currentTime;
  }
  // Integrate rate of change of quaternion to yield quaternion
  q += qDot * dt;

  // Normalise quaternion
  q.normalise();
}

//-----------------------------------------------------------------------------
// IMU algorithm update

void MadgwickAHRS::updateIMU(float gx, float gy, float gz, float ax, float ay,
                             float az) {
  Quaternion s, qDot, a;
  float q0q0, q1q1, q2q2, q3q3;

  // Rate of change of quaternion from gyroscope
  qDot = 0.5 * (q * Quaternion(0, gx, gy, gz));

  // Compute feedback only if accelerometer measurement valid (avoids NaN in
  // accelerometer normalisation)
  if (!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f))) {

    // Normalise accelerometer measurement
    a = Quaternion(0, ax, ay, az).normalise();

    // Auxiliary variables to avoid repeated arithmetic
    q0q0 = q[0] * q[0];
    q1q1 = q[1] * q[1];
    q2q2 = q[2] * q[2];
    q3q3 = q[3] * q[3];

    // Gradient decent algorithm corrective step
    s[0] = (4.0f * q[0] * q2q2) + (2.0f * q[2] * a[1]) + (4.0f * q[0] * q1q1) -
           (2.0f * q[1] * a[2]);
    s[1] = (4.0f * q[1] * q3q3) - (2.0f * q[3] * a[1]) + (4.0f * q0q0 * q[1]) -
           (2.0f * q[0] * a[2]) - 4.0f * q[1] + (8.0f * q[1] * q1q1) +
           (8.0f * q[1] * q2q2) + (4.0f * q[1] * a[3]);
    s[2] = (4.0f * q0q0 * q[2]) + (2.0f * q[0] * a[1]) + (4.0f * q[2] * q3q3) -
           (2.0f * q[3] * a[2]) - 4.0f * q[2] + (8.0f * q[2] * q1q1) +
           (8.0f * q[2] * q2q2) + (4.0f * q[2] * a[3]);
    s[3] = (4.0f * q1q1 * q[3]) - (2.0f * q[1] * a[1]) + (4.0f * q2q2 * q[3]) -
           (2.0f * q[2] * a[2]);

    // Apply feedback step
    qDot -= s.normalise() * beta;
  }

  // calculate the time passed
  float dt = sampleInterval;
  if (dt == 0.0f) {
    unsigned long currentTime = micros();
    dt = (currentTime - lastSampleTime) / 1000000.0f;
    lastSampleTime = currentTime;
  }
  // Integrate rate of change of quaternion to yield quaternion
  q += qDot * dt;

  // Normalise quaternion
  q.normalise();
}
