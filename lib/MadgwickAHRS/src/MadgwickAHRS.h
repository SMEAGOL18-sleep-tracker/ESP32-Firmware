/*
    MadgwickAHRS.cpp
    Author: Benedikt Eliasson

    Arduino C++ class-style implementation of Madgwick's IMU and AHRS
   algorithms. See:
   http://www.x-io.co.uk/node/8#open_source_ahrs_and_imu_algorithms
*/

#ifndef ArduinoMadgwickAHRS_h
#define ArduinoMadgwickAHRS_h

#include "Quaternion.h"
#include <Arduino.h>

class MadgwickAHRS {
public:
  MadgwickAHRS(float beta = 0.2f, float interval = 0.0f);
  ~MadgwickAHRS() = default;

  void update(float gx, float gy, float gz, float ax, float ay, float az,
              float mx, float my, float mz);
  void updateIMU(float gx, float gy, float gz, float ax, float ay, float az);

  Quaternion getRotation(void) { return q; };

private:
  Quaternion q = Quaternion();
  const float beta = 0.2f;
  float sampleInterval = 0.0f;
  unsigned long lastSampleTime = 0;
};
#endif
