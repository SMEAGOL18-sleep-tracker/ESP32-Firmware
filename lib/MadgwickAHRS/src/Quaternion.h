/*
    Quaternion.h
    Author: Benedikt Eliasson

    C++ library for basic usage of quaternions.
*/

#ifndef Quaternion_h
#define Quaternion_h

#include <iostream>

//------------------------------------------------------------------------------
// Definitions

typedef struct {
  float roll;  /* rotation around x axis in degrees */
  float pitch; /* rotation around y axis in degrees */
  float yaw;   /* rotation around z axis in degrees */
} EulerAnglesStruct;

typedef struct {
  float x; /* rotation around x axis in degrees */
  float y; /* rotation around y axis in degrees */
  float z; /* rotation around z axis in degrees */
} VectorStruct;

//------------------------------------------------------------------------------
// Class declaration

class Quaternion {
public:
  Quaternion(void);
  Quaternion(const float w, const float x, const float y, const float z);

  Quaternion &normalise(void);

  Quaternion conj(void) const;
  EulerAnglesStruct getEulerAngles(void) const;

  VectorStruct rotateVector(float x, float y, float z);

  float operator[](unsigned int index) const;
  float &operator[](unsigned int index);
  bool operator==(const Quaternion &rhs) const;

  Quaternion operator+(const Quaternion &rhs);
  Quaternion operator-(const Quaternion &rhs);
  Quaternion &operator+=(const Quaternion &rhs);
  Quaternion &operator-=(const Quaternion &rhs);

  Quaternion operator*(float rhs);
  Quaternion &operator*=(float rhs);
  friend Quaternion operator*(float lhs, const Quaternion &rhs);
  Quaternion operator*(const Quaternion &rhs);

  friend std::ostream &operator<<(std::ostream &stream,
                                  const Quaternion &quaternion) {
    stream << "Quaternion:{" << quaternion[0] << ", " << quaternion[1] << ", "
           << quaternion[2] << ", " << quaternion[3] << "}";
    return stream;
  }

private:
  float q[4];
};

#endif

//------------------------------------------------------------------------------
// End of file
