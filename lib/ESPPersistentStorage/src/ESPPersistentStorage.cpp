#include "ESPPersistentStorage.h"

ESPPersistentStorage::ESPPersistentStorage(const String &fileName) : _fileName(fileName) {
}

uint8_t ESPPersistentStorage::set(const String &key, uint8_t *value, uint8_t value_len) {
  if (!SPIFFS.exists(_fileName)) {
    SPIFFS.open(_fileName, "w+");
  }
  File dataFile = SPIFFS.open(_fileName, "r+");
  if (!dataFile) {
    return 0;
  }
  uint32_t index = _seekKey(&dataFile, key);

  if (index == dataFile.size()) {
    dataFile.seek(index);
    uint8_t key_len = key.length() > PS_MAX_KEY_LENGTH ? PS_MAX_KEY_LENGTH : key.length();
    key_len         = dataFile.write((uint8_t *)key.c_str(), key_len);
    while (key_len < PS_MAX_KEY_LENGTH) {
      key_len += dataFile.write('\0');
    }
    dataFile.write('\t');
  }

  value_len = value_len > PS_MAX_VALUE_LENGTH ? PS_MAX_VALUE_LENGTH : value_len;
  dataFile.seek(index + PS_MAX_KEY_LENGTH + 1);
  // Serial.println(String("set ") + key + (index + PS_MAX_KEY_LENGTH + 1));
  uint8_t i = 0;
  int c     = dataFile.peek();
  while (i < value_len) {
    if (c < 0 || value[i] != (uint8_t)c) {
      dataFile.write(value[i]);
      c = dataFile.peek();
    } else {
      c = dataFile.read();
    }
    i++;
  }

  while (value_len < PS_MAX_VALUE_LENGTH) {
    value_len += dataFile.write(0);
  }
  dataFile.write('\n');

  dataFile.flush();
  dataFile.close();
  return value_len;
}

uint8_t ESPPersistentStorage::get(const String &key, uint8_t *value, uint8_t value_len) {
  File dataFile = SPIFFS.open(_fileName, "a+");
  if (!dataFile) {
    return 0;
  }

  uint32_t index = _seekKey(&dataFile, key);
  if (index == dataFile.size())
    return 0;

  // limit the max value len
  value_len = value_len > PS_MAX_VALUE_LENGTH ? PS_MAX_VALUE_LENGTH : value_len;
  dataFile.seek(index + PS_MAX_KEY_LENGTH + 1);
  // Serial.println(String("get ") + key + (index + PS_MAX_KEY_LENGTH + 1));
  uint8_t i = 0;
  if (dataFile.position() < dataFile.size()) {
    int c = dataFile.read();
    while (i < value_len && c >= 0) {
      value[i++] = (uint8_t)c;
      c          = dataFile.read();
    }
  }

  dataFile.close();
  return i;
}

uint32_t ESPPersistentStorage::_seekKey(File *file, const String &key) {
  const uint32_t start = _lastPosition;
  uint8_t keylen       = key.length() > PS_MAX_KEY_LENGTH ? PS_MAX_KEY_LENGTH : key.length();
  bool wrapped         = false;
  uint8_t i            = 0;
  int c                = 0;

  while (!wrapped || _lastPosition <= start) {
    if (!wrapped && _lastPosition >= file->size()) {
      _lastPosition = 0;
      wrapped       = true;
    }

    file->seek(_lastPosition);
    // read the key and compare it against the given key
    i = 0;
    c = file->read();
    while (c >= 0 && c != '\0' && c != '\t' && i < keylen && key[i] == (char)c) {
      c = file->read();
      i++;
    }

    // we found it
    if (i == keylen && c >= 0) {
      return _lastPosition;
    }
    _lastPosition += PS_MAX_LENGTH;
  };
  _lastPosition = file->size();
  return _lastPosition;
}

void ESPPersistentStorage::clear(void) {
  if (SPIFFS.exists(_fileName)) {
    SPIFFS.remove(_fileName);
  }
}
