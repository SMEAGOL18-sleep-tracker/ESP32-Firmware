#ifndef ESPPersistentStorage_H
#define ESPPersistentStorage_H

#include <WString.h>

#include <SPIFFS.h>

#define PS_MAX_KEY_LENGTH 16
#define PS_MAX_VALUE_LENGTH 16

#define PS_MAX_LENGTH (PS_MAX_KEY_LENGTH + PS_MAX_VALUE_LENGTH + 2)

/**
   * \class ESPPersistentStorage
   *
   * \brief A class for managing persistent storage of variables which
   *        won't get deleted on a system reboot.
   *        Data should be written rarely since it wears off the flash memory
   *
   * \author $Author: Benedikt Eliasson$
   * Contact: Benedikt@Rysta.io
   *
   */

class ESPPersistentStorage {
public:
  ESPPersistentStorage(const String &fileName = "data.persistent");
  ~ESPPersistentStorage() = default;

  uint8_t set(const String &key, uint8_t *value, uint8_t value_len);
  uint8_t get(const String &key, uint8_t *value, uint8_t value_len);

  template <typename T>
  inline uint8_t set(const String &key, T *value, uint8_t value_len = sizeof(T)) {
    return set(key, (uint8_t *)value, value_len);
  };
  template <typename T>
  inline uint8_t get(const String &key, T *value, uint8_t value_len = sizeof(T)) {
    return get(key, (uint8_t *)value, value_len);
  };

  void clear(void);

private:
  uint32_t _seekKey(File *file, const String &key);

private:
  const String _fileName;
  uint32_t _lastPosition = 0;
};

#endif   // ESPPersistentStorage_H
